#define FunctionsRunnerMaxFuncs 4

typedef struct FunctionsRunnerThreadInfo {
  bool isInitialized;
  pthread_t id;
  void *args;
} FunctionsRunnerThreadInfo;

typedef struct FunctionsRunner {
  FunctionsRunnerThreadInfo threadsInfo[FunctionsRunnerMaxFuncs];
  bool functionsRunningCount;
} FunctionsRunner;

typedef struct FunctionsRunnerThreadInput {
    FunctionsRunner *functionsRunner;
    FunctionsRunnerThreadInfo *threadInfo;
    Any args;
} FunctionsRunnerThreadInput;

FunctionsRunnerThreadInput FunctionsRunnerThreadInput_New(
  FunctionsRunner *functionsRunner,
  FunctionsRunnerThreadInfo *threadInfo,
  Any args
) {
  FunctionsRunnerThreadInput functionsRunnerThreadInput;
  functionsRunnerThreadInput.functionsRunner = functionsRunner;
  functionsRunnerThreadInput.threadInfo = threadInfo;
  functionsRunnerThreadInput.args = args;
  return functionsRunnerThreadInput;
};

FunctionsRunnerThreadInfo FunctionsRunnerThreadInfo_New() {
  FunctionsRunnerThreadInfo functionsRunnerThreadInfo;
  functionsRunnerThreadInfo.isInitialized = false;
  return functionsRunnerThreadInfo;
};

FunctionsRunner FunctionsRunner_New() {
  FunctionsRunner functionsRunner;
  for (int i = 0; i < FunctionsRunnerMaxFuncs; i++) {
    functionsRunner.threadsInfo[i] = FunctionsRunnerThreadInfo_New();
  }
  functionsRunner.functionsRunningCount = 0;
  return functionsRunner;
};

void FunctionsRunner_RunFunction(
  FunctionsRunner *functionsRunner,
  void *(*functionPointer)(void *),
  void *restrict functionArgs
) {
  for (int i = 0; i < FunctionsRunnerMaxFuncs; i++) {
    FunctionsRunnerThreadInfo *threadInfo = &functionsRunner->threadsInfo[i];
    if (threadInfo->isInitialized == false) {
      threadInfo->isInitialized = true;
      FunctionsRunnerThreadInput input = FunctionsRunnerThreadInput_New(
        functionsRunner,
        threadInfo,
        functionArgs
      );
      pthread_create(&threadInfo->id, NULL, functionPointer, &input);
      return;
    }
  }
};
