void ThreadSafe_FunctionsRunnerFreeThreadSlotAndStopIfNeeded(
  FunctionsRunner *functionsRunner,
  FunctionsRunnerThreadInfo *threadInfo
)
{
  // TODO
  printf("ThreadSafe_FunctionsRunnerFreeThreadSlotAndStopIfNeeded\n");
  exit(0);
};

void ThreadSafe_ThreadDidFinish(
  FunctionsRunner *functionsRunner,
  FunctionsRunnerThreadInfo *threadInfo,
  void *result
)
{
  ThreadSafe_FunctionsRunnerFreeThreadSlotAndStopIfNeeded(
    functionsRunner,
    threadInfo
  );
};
