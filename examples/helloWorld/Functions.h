Any Function_HelloWorld(Any args) {
  FunctionsRunnerThreadInput *input = (FunctionsRunnerThreadInput *) args;
  printf("Hello World\n");
  ThreadSafe_ThreadDidFinish(
    input->functionsRunner,
    input->threadInfo,
    NULL
  );
  return NULL;
};
