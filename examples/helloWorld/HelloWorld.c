#include <stdio.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <Helpers.h>
#include <FunctionsRunner.h>
#include <ThreadSafe.h>

#include "Functions.h"

int main(int argc, StringArray argv) {

  FunctionsRunner functionsRunner = FunctionsRunner_New();
  FunctionsRunner_RunFunction(&functionsRunner, Function_HelloWorld, NULL);

  while (true) {};

  return 0;

};
